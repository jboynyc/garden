#!/usr/bin/env nix-shell
#! nix-shell -p phodav -i bash

echo Serving at http://localhost:8080
chezdav --local -p 8080 -P .
