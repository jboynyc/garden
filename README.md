# digital garden repository

- [Tiddlywiki][] for gardening
- [phởdav][] for local editing
- [GitLab Pages][] for publishing

[Tiddlywiki]: https://tiddlywiki.com/
[phởdav]: https://wiki.gnome.org/phodav
[GitLab Pages]: https://docs.gitlab.com/ee/user/project/pages/
